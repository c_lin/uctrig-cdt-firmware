--
----  2003 by Mircea Bogdan
--

title "vme block";

include "lpm_counter.inc";

subdesign tdc_vme

(
   -- VME I/O
  addr[26..2]     	: input;      -- address (minus GA, unused 26-24,1-0)
  data[31..0]     	: bidir;      -- vme data lines
  rd_data0[31..0] 	: input;		-- data from memory0 for VME read-out
  rd_data1[31..0] 	: input;
  rd_data2[31..0] 	: input;
  rd_data3[31..0] 	: input;
  rd_data4[31..0] 	: input;
  rd_data5[31..0] 	: input;
  rd_data6[31..0] 	: input;
  rd_data7[31..0] 	: input;
  rd_data8[31..0] 	: input;
  status_bus[31..0]	: input;
  ex_status_bus[31..0]	: input;
  vmewrite        	: input;      -- qualifies address value (0=Read, 1=Write)
  vmeas           	: input;      -- signal for latching address - used for data also, in Pulsar
  vmeds           	: input;      -- signal for latching data - not used in Pulsar
	
  test_mode       	: output;     -- puts the chip in test mode: write memories, registers,etc.
  reset           	: output;     -- resets the chip
  test_data_mode	: output;     -- switch from real data to test data
  halt_mode			: output;     -- freeze content of memories
  vmel2a_mode		: output;     -- switch level2 accept from DAQ to VME
  vme_ctrlwrite7_out	: output; -- will generate the pulse for VME leve2 accept

  register1[31..0]	: output;     -- VME accessible control register
  register2[31..0]	: output;     -- same
  register3[31..0]	: output;     -- same
  register4[31..0]	: output;     -- same
  register5[31..0]	: output;     -- same
  register6[31..0]	: output;     -- same
  register7[31..0]	: output;     -- same
  register8[31..0]	: output;     -- same
  vme_wr_en0      	: output;	  -- enable VME write on memory0
  vme_wr_en1     	: output;
  vme_wr_en2     	: output;
  vme_wr_en3      	: output;
  vme_wr_en6     	: output;
  vme_wr_en7     	: output;
  vme_wr_en8      	: output;


  id		        : input;      -- which chip am I? (2 chips on board)
  dip[9..0]       	: input;      -- board ID DIP switch
  sysclk          	: input;      -- VME system clock or other clock(possibly useful)
)
variable
  testdatamodereg  : dff;		-- test data mode register
  testdatamode     : node;      -- alias for test data mode register
  haltmodereg      : dff;       -- halt mode register
  haltmode         : node;      -- alias for halt mode register
  testmodereg      : dff;       -- test mode register
  testmode         : node;      -- alias for test mode register
  vmel2amodereg    : dff;       -- vmel2a mode register
  vmel2amode       : node;      -- alias for vmel2a mode register
  ctrlreg1[31..0]  : dff;		-- misc control registers
  ctrlreg2[31..0]  : dff;		-- misc control registers
  ctrlreg3[31..0]  : dff;		-- misc control registers
  ctrlreg4[31..0]  : dff;		-- misc control registers
  ctrlreg5[31..0]  : dff;		-- misc control registers
  ctrlreg6[31..0]  : dff;		-- misc control registers
  ctrlreg7[31..0]  : dff;		-- misc control registers
  ctrlreg8[31..0]  : dff;		-- misc control registers
 
  -- tri-state buffers for VME data lines
  datatri[31..0]  : tri_state_node;
  dataout[31..0]  : tri;        -- vme readout gets muxed here

  -- nodes holding results of VME address decoding
  vme_reset       : node;		-- reset output for chip
  vme_testmodewrite : node;     -- write test mode register
  vme_testdatamodewrite : node;     -- write test mode register
  vme_haltmodewrite : node;     -- write test mode register
  vme_vmel2amodewrite : node;   -- write vmel2a mode register
  vme_ctrlwrite1   : node;		-- write control register1
  vme_ctrlwrite2   : node;		-- write control register2
  vme_ctrlwrite3   : node;		-- write control register3
  vme_ctrlwrite4   : node;		-- write control register4
  vme_ctrlwrite5   : node;		-- write control register5
  vme_ctrlwrite6   : node;		-- write control register6
  vme_ctrlwrite7   : node;		-- write control register7
  vme_ctrlwrite8   : node;		-- write control register8

begin
 
  -- drive VME data lines from tri-state buffers
  data[] = datatri[];
  datatri[] = dataout[].out;


  -- put chip in "test-mode" by writing H"1" on address H"F"
  -- put chip in "operation-mode" by writing H"0" on address H"F"
  vme_testmodewrite = vmeas & vmewrite & (addr[26..17]==H"0") & (addr[15..2]==H"F");
  testmodereg.d = data[0];
  testmodereg.clk =  vme_testmodewrite;
  testmode = testmodereg.q;

  -- put chip in "test-data-mode" by writing H"1" on address H"D"
  -- put chip in "real-data-mode" by writing H"0" on address H"D"
  vme_testdatamodewrite = vmeas & vmewrite & (addr[26..17]==H"0") & (addr[15..2]==H"D");
  testdatamodereg.d = data[0];
  testdatamodereg.clk =  vme_testdatamodewrite;
  testdatamode = testdatamodereg.q;

 -- put chip in "halt-mode" by writing H"1" on address H"C"
  -- put chip in "run-mode" by writing H"0" on address H"C"
  vme_haltmodewrite = vmeas & vmewrite & (addr[26..17]==H"0") & (addr[15..2]==H"C");
  haltmodereg.d = data[0];
  haltmodereg.clk =  vme_haltmodewrite;
  haltmode = haltmodereg.q;

  -- put chips in "vmel2-mode" by writing H"1" on address H"B"
  -- put chips in "daql2-mode" by writing H"1" on address H"0"
  vme_vmel2amodewrite = vmeas & vmewrite & (addr[26..17]==H"0") & (addr[15..2]==H"B");
  vmel2amodereg.d = data[0];
  vmel2amodereg.clk =  vme_vmel2amodewrite;
  vmel2amode = vmel2amodereg.q;

  -- write registers via vme - (chip has to be in "test-mode", except for reister7)
  vme_ctrlwrite1 = vmeas & vmewrite &(testmode==H"1")&(addr[16]==id)&(addr[26..17]==H"0")&(addr[15..2]==H"1");
  vme_ctrlwrite2 = vmeas & vmewrite &(testmode==H"1")&(addr[16]==id)&(addr[26..17]==H"0")&(addr[15..2]==H"2");
  vme_ctrlwrite3 = vmeas & vmewrite &(testmode==H"1")&(addr[16]==id)&(addr[26..17]==H"0")&(addr[15..2]==H"3");
  vme_ctrlwrite4 = vmeas & vmewrite &(testmode==H"1")&(addr[16]==id)&(addr[26..17]==H"0")&(addr[15..2]==H"4");
  vme_ctrlwrite5 = vmeas & vmewrite &(testmode==H"1")&(addr[16]==id)&(addr[26..17]==H"0")&(addr[15..2]==H"5");
  vme_ctrlwrite6 = vmeas & vmewrite &(testmode==H"1")&(addr[16]==id)&(addr[26..17]==H"0")&(addr[15..2]==H"6");
  vme_ctrlwrite7 = vmeas & vmewrite &(addr[26..17]==H"0")&(addr[15..2]==H"7");
  vme_ctrlwrite8 = vmeas & vmewrite &(testmode==H"1")&(addr[16]==id)&(addr[26..17]==H"0")&(addr[15..2]==H"8");

  ctrlreg1[].d = data[];
  ctrlreg1[].clk = vme_ctrlwrite1;
  ctrlreg2[].d = data[];
  ctrlreg2[].clk = vme_ctrlwrite2;
  ctrlreg3[].d = data[];
  ctrlreg3[].clk = vme_ctrlwrite3;
  ctrlreg4[].d = data[];
  ctrlreg4[].clk = vme_ctrlwrite4;
  ctrlreg5[].d = data[];
  ctrlreg5[].clk = vme_ctrlwrite5;
  ctrlreg6[].d = data[];
  ctrlreg6[].clk = vme_ctrlwrite6;
  ctrlreg7[].d = data[];
  ctrlreg7[].clk = vme_ctrlwrite7;
  ctrlreg8[].d = data[];
  ctrlreg8[].clk = vme_ctrlwrite8;
  
  -- write memories via vme - (chip has to be in "test-mode")
 
  vme_wr_en0 = vmewrite & (addr[26]==H"1") & (testmode==H"1");
  vme_wr_en1 = vmewrite & (addr[26..20]==H"9") & (testmode==H"1");
  vme_wr_en2 = vmewrite & (addr[26..20]==H"7") & (testmode==H"1");
  vme_wr_en3 = vmewrite & (addr[26..20]==H"1") & (testmode==H"1");
  vme_wr_en6 = vmewrite & (addr[26..20]==H"C") & (testmode==H"1");
  vme_wr_en7 = vmewrite & (addr[26..20]==H"D") & (testmode==H"1");
  vme_wr_en8 = vmewrite & (addr[26..20]==H"E") & (testmode==H"1");


  -- do reset via vme (chip has to be in "test-mode") - works on bouth chips at once
  vme_reset = vmeas & vmewrite & (addr[26..17]==H"0")&(testmode==H"1")&(addr[15..2]==H"11");

  -- read memorys via vme
  if (addr[26]==H"1") then
    dataout[].in = rd_data0[];
  end if;
  if (addr[26..20]==H"9") then
    dataout[].in = rd_data1[];
  end if;
  if (addr[26..20]==H"7") then
    dataout[].in = rd_data2[];
  end if;
  if (addr[26..20]==H"1") then
    dataout[].in = rd_data3[];
  end if;
  if (addr[26..20]==H"A") then
    dataout[].in = rd_data4[];
  end if;
  if (addr[26..20]==H"B") then
    dataout[].in = rd_data5[];
  end if;
  if (addr[26..20]==H"C") then
    dataout[].in = rd_data6[];
  end if;
  if (addr[26..20]==H"D") then
    dataout[].in = rd_data7[];
  end if;
  if (addr[26..20]==H"E") then
    dataout[].in = rd_data8[];
  end if;

  -- read DEADBE and status registers via vme
  if ((addr[26..17]==H"0") & (addr[15..2]==H"0")) then 
    dataout[31..8].in = H"deadbe";
    dataout[7].in = GND;
    dataout[6].in = GND;
    dataout[5].in = GND;
    dataout[4].in = vmel2amode;
    dataout[3].in = haltmode;
    dataout[2].in = testmode;
    dataout[1].in = testdatamode;
    dataout[0].in = id;
  end if;
  
  -- read status_bus via vme
  if ((addr[26..17]==H"0") & (addr[15..2]==H"A")) then 
    dataout[].in = status_bus[];
  end if;

 -- read ex_status_bus via vme
  if ((addr[26..17]==H"0") & (addr[15..2]==H"9")) then 
    dataout[].in = ex_status_bus[];
  end if;

  -- read control reg via vme
  if ((addr[26..17]==H"0") & (addr[15..2]==H"1")) then
    dataout[].in = ctrlreg1[].q;
  end if;
  if ((addr[26..17]==H"0") & (addr[15..2]==H"2")) then
    dataout[].in = ctrlreg2[].q;
  end if;
  if ((addr[26..17]==H"0") & (addr[15..2]==H"3")) then
    dataout[].in = ctrlreg3[].q;
  end if;
  if ((addr[26..17]==H"0") & (addr[15..2]==H"4")) then
    dataout[].in = ctrlreg4[].q;
  end if;
   if ((addr[26..17]==H"0") & (addr[15..2]==H"5")) then
    dataout[].in = ctrlreg5[].q;
  end if;
  if ((addr[26..17]==H"0") & (addr[15..2]==H"6")) then
    dataout[].in = ctrlreg6[].q;
  end if;
  if ((addr[26..17]==H"0") & (addr[15..2]==H"7")) then
    dataout[].in = ctrlreg7[].q;
  end if;
  if ((addr[26..17]==H"0") & (addr[15..2]==H"8")) then
    dataout[].in = ctrlreg8[].q;
  end if;

   -- put data on the vme bus
   -- (I left addr[17]==H"1" for the VME chip CSR block.)
  dataout[].oe = vmeas & !vmewrite & (addr[17]==H"0") & addr[16]==id;

  -- reset chip via vme
  reset = vme_reset;

  -- output VME registers for use inside big chip
  test_mode = testmode;  -- this is "H" if you write "1" at addr "F" and it stays like that
                         -- until you write "0" at addr "F".
  test_data_mode = testdatamode;  -- this is "H" if you write "1" at addr "D" and it stays like that
                         -- until you write "0" at addr "D".
  halt_mode = haltmode;  -- this is "H" if you write "1" at addr "C" and it stays like that
                         -- until you write "0" at addr "C".
  vmel2a_mode = vmel2amode;  -- this is "H" if you write "1" at addr "B" and it stays like that
                         -- until you write "0" at addr "B".
  vme_ctrlwrite7_out = vme_ctrlwrite7;

  register1[] = ctrlreg1[].q;  
  register2[] = ctrlreg2[].q;
  register3[] = ctrlreg3[].q;  
  register4[] = ctrlreg4[].q;
  register5[] = ctrlreg5[].q;  
  register6[] = ctrlreg6[].q;
  register7[] = ctrlreg7[].q;  
  register8[] = ctrlreg8[].q;

end;
