/* 
   C. Lin, chiehlin@uchicago.edu
*/

module trig_counter
(
// input 
  clk               , // system clock
  reset             ,
  
  // inputs
  in                ,
   
  // output
  cnt                       
              
);

input wire         clk;

// inputs
input wire         in;
input wire         reset;

// output
output reg [31 :0] cnt;


////////////////////////////////////////////
always @(posedge clk) begin

   if( reset ) cnt <= 0;
   if( in ) cnt <= cnt + 1;
		
end

endmodule