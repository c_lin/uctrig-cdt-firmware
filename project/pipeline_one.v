/* 
   rising edge
   C. Lin, chiehlin@uchicago.edu
*/

module pipeline_one
(
// input 
  clk               , // system clock
  reset             ,
  
  // inputs
  in                ,
  depth             ,
   
  // output
  q                       
              
);

input wire         clk;

// inputs
input wire         in;
input wire         reset;
input wire  [9 :0] depth;

// output
output reg         q;

reg [1023:0] pipeline;

////////////////////////////////////////////
always @(posedge clk) begin

   pipeline = (reset) ? 0 : (pipeline << 1 ); 
   pipeline[0] = in;
   q = pipeline[depth];
		
end

endmodule