-- megafunction wizard: %ALTACCUMULATE%
-- GENERATION: STANDARD
-- VERSION: WM1.0
-- MODULE: altaccumulate 

-- ============================================================
-- File Name: CL_ACCUM_2.vhd
-- Megafunction Name(s):
-- 			altaccumulate
--
-- Simulation Library Files(s):
-- 			altera_mf
-- ============================================================
-- ************************************************************
-- THIS IS A WIZARD-GENERATED FILE. DO NOT EDIT THIS FILE!
--
-- 9.1 Build 350 03/24/2010 SP 2 SJ Full Version
-- ************************************************************


--Copyright (C) 1991-2010 Altera Corporation
--Your use of Altera Corporation's design tools, logic functions 
--and other software and tools, and its AMPP partner logic 
--functions, and any output files from any of the foregoing 
--(including device programming or simulation files), and any 
--associated documentation or information are expressly subject 
--to the terms and conditions of the Altera Program License 
--Subscription Agreement, Altera MegaCore Function License 
--Agreement, or other applicable license agreement, including, 
--without limitation, that your use is for the sole purpose of 
--programming logic devices manufactured by Altera and sold by 
--Altera or its authorized distributors.  Please refer to the 
--applicable agreement for further details.


LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY altera_mf;
USE altera_mf.all;

ENTITY CL_ACCUM_2 IS
	PORT
	(
		aclr		: IN STD_LOGIC  := '0';
		clken		: IN STD_LOGIC  := '1';
		clock		: IN STD_LOGIC  := '0';
		data		: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
END CL_ACCUM_2;


ARCHITECTURE SYN OF cl_accum_2 IS

	SIGNAL sub_wire0	: STD_LOGIC_VECTOR (15 DOWNTO 0);



	COMPONENT altaccumulate
	GENERIC (
		lpm_representation		: STRING;
		lpm_type		: STRING;
		width_in		: NATURAL;
		width_out		: NATURAL
	);
	PORT (
			clken	: IN STD_LOGIC ;
			aclr	: IN STD_LOGIC ;
			clock	: IN STD_LOGIC ;
			data	: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
			result	: OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
	);
	END COMPONENT;

BEGIN
	result    <= sub_wire0(15 DOWNTO 0);

	altaccumulate_component : altaccumulate
	GENERIC MAP (
		lpm_representation => "UNSIGNED",
		lpm_type => "altaccumulate",
		width_in => 8,
		width_out => 16
	)
	PORT MAP (
		clken => clken,
		aclr => aclr,
		clock => clock,
		data => data,
		result => sub_wire0
	);



END SYN;

-- ============================================================
-- CNX file retrieval info
-- ============================================================
-- Retrieval info: PRIVATE: ACLR NUMERIC "1"
-- Retrieval info: PRIVATE: ADD_SUB NUMERIC "0"
-- Retrieval info: PRIVATE: CIN NUMERIC "0"
-- Retrieval info: PRIVATE: CLKEN NUMERIC "1"
-- Retrieval info: PRIVATE: COUT NUMERIC "0"
-- Retrieval info: PRIVATE: EXTRA_LATENCY NUMERIC "0"
-- Retrieval info: PRIVATE: INTENDED_DEVICE_FAMILY STRING "Stratix II"
-- Retrieval info: PRIVATE: LATENCY NUMERIC "0"
-- Retrieval info: PRIVATE: LPM_REPRESENTATION NUMERIC "1"
-- Retrieval info: PRIVATE: OVERFLOW NUMERIC "0"
-- Retrieval info: PRIVATE: SLOAD NUMERIC "0"
-- Retrieval info: PRIVATE: SYNTH_WRAPPER_GEN_POSTFIX STRING "0"
-- Retrieval info: PRIVATE: WIDTH_IN NUMERIC "8"
-- Retrieval info: PRIVATE: WIDTH_OUT NUMERIC "16"
-- Retrieval info: CONSTANT: LPM_REPRESENTATION STRING "UNSIGNED"
-- Retrieval info: CONSTANT: LPM_TYPE STRING "altaccumulate"
-- Retrieval info: CONSTANT: WIDTH_IN NUMERIC "8"
-- Retrieval info: CONSTANT: WIDTH_OUT NUMERIC "16"
-- Retrieval info: USED_PORT: aclr 0 0 0 0 INPUT GND aclr
-- Retrieval info: USED_PORT: clken 0 0 0 0 INPUT VCC clken
-- Retrieval info: USED_PORT: clock 0 0 0 0 INPUT GND clock
-- Retrieval info: USED_PORT: data 0 0 8 0 INPUT NODEFVAL data[7..0]
-- Retrieval info: USED_PORT: result 0 0 16 0 OUTPUT NODEFVAL result[15..0]
-- Retrieval info: CONNECT: @data 0 0 8 0 data 0 0 8 0
-- Retrieval info: CONNECT: result 0 0 16 0 @result 0 0 16 0
-- Retrieval info: CONNECT: @clock 0 0 0 0 clock 0 0 0 0
-- Retrieval info: CONNECT: @clken 0 0 0 0 clken 0 0 0 0
-- Retrieval info: CONNECT: @aclr 0 0 0 0 aclr 0 0 0 0
-- Retrieval info: LIBRARY: altera_mf altera_mf.altera_mf_components.all
-- Retrieval info: GEN_FILE: TYPE_NORMAL CL_ACCUM_2.vhd TRUE
-- Retrieval info: GEN_FILE: TYPE_NORMAL CL_ACCUM_2.inc TRUE
-- Retrieval info: GEN_FILE: TYPE_NORMAL CL_ACCUM_2.cmp FALSE
-- Retrieval info: GEN_FILE: TYPE_NORMAL CL_ACCUM_2.bsf TRUE FALSE
-- Retrieval info: GEN_FILE: TYPE_NORMAL CL_ACCUM_2_inst.vhd FALSE
-- Retrieval info: GEN_FILE: TYPE_NORMAL CL_ACCUM_2_waveforms.html TRUE
-- Retrieval info: GEN_FILE: TYPE_NORMAL CL_ACCUM_2_wave*.jpg FALSE
-- Retrieval info: LIB_FILE: altera_mf
