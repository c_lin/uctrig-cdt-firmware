/* 
   rising edge
   C. Lin, chiehlin@uchicago.edu
*/

module send_delta
(
// input 
  clk               , // system clock
  
  // inputs
  start             , 
   
  // output
  sel               ,
  q                        
              
);

input wire         clk;

// inputs
input wire         start;

// output
output reg         q;
output reg         sel;

reg [2 :0] cnt;

////////////////////////////////////////////
always @(posedge clk) begin

   if( start ) begin
      cnt = 0;
      sel = 1'b0;
   end
   
   case( cnt )
      3: q = 1'b1;
      4: q = 1'b0;
      5: q = 1'b0;
      6: q = 1'b1;
      default: q = 1'b0;
   endcase
   
   if( cnt < 7 ) begin
      cnt = cnt + 1;
   end
   else begin
      sel =  1'b1;
   end
		
end

endmodule